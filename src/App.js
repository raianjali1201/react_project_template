import React from 'react';

function App() {
  return (
    <>
    <div className = "App">
      <header className="App-Header">
        <button type ="button" className = "btn btn-primary">
          Primary Button
        </button>
      </header>
    </div>
    </>
  );
}

export default App;
